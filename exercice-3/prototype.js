
const ojb = {
    p0: "2ZZ",
    p1: function(){
        console.log("hello");
        
    },
    p2() {
        console.log("hellohello");
        
    },
    p3: ()=>{
        console.log("logloglog");
        
    }

    
}

const Student = function(name) {
	//privé
	const sname = name;
	const getName = () => sname;
	//privilégié
	this.displayName = () => {
		console.log(sname);
	}
	//public
	this.sayHello = function() {
		console.log("hello");
	}
};
const stud1 = new Student('Riri');
stud1.displayName();
// create a new function
Student.prototype.whereIAm = () => "in the kitchen";
//stud1.getName();
//stud1.__proto__.sayHello();
console.log(stud1.whereIAm());


String.prototype.ucfirst = function () {
    return this[0].toUpperCase() + this.substring(1);	
};


console.log("log".ucfirst());

const objet = {
    animal: {
        type: {
        name: "chien"
        },
    },
    };

Object.prototype.prop_access = function (params) {
    var a = this;
    for (let index = 0; index < params.toLowerCase().split('.').length; index++) {

        var option = params.toLowerCase().split('.')[index];
        a = a[option];

    }
    return a;
}
console.log(objet.prop_access("animal.type.name"));


