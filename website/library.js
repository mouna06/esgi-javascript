
export function ucfirst(str){
	if(typeof str !== "string" || !str) return '';
	return str[0].toUpperCase() + str.substring(1);
}

export default function capitalize(str){
	if(typeof str !== "string" || !str) return '';
	return str.toLowerCase().split(" ").map(function(item){
		return ucfirst(item);
	}).join(" ");
		
}