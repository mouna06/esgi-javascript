const getStudents = function(resolve, reject) {
  setTimeout(() => {resolve([{name: "Luc"}, {name: "Charles"}])}, 7000);
  
}

const getCourses = function(resolve, reject) {
  setTimeout(() => {resolve([{name: "JS"}, {name: "PHP"}])}, 10000);
  
}

const timer = function(resolve, reject) {
  setTimeout(() => resolve('timeout'), 2000);
}

Promise.race([
Promise.all([new Promise(getCourses), new Promise(getStudents)])
.then(result => {
  const students = result[0];
  const courses = result[1];

  console.log('mapped');
})
, new Promise(timer)
]).then((result) => {
   console.log(result);
})